var far = 3000;
var near = -3000;
var cz = 2000;
var views = [
  {
    left: 0.0,
    top: 0,
    width: 0.5,
    height: 0.5,
    background: new THREE.Color( 0x000000 ),
    eye: [ 0, 0, 1000 ],
    up: [ 0, 0, 0 ],
    fov: 30,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.z -= mouseX * 0.005;
      camera.position.z = Math.max( Math.min( camera.position.z, far ), near );
      camera.lookAt( scene.position );
    }
  },
  {
    left: 0.0,
    top: 0.5,
    width: 0.5,
    height: 0.5,
    background: new THREE.Color( 0x000000 ),
    eye: [ 0, 0, 480],
    up: [ 0, 0, 0 ],
    fov: 30,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.z -= mouseX * 0.005;
     camera.position.z = Math.max( Math.min( camera.position.z, far ), near );
     camera.lookAt( scene.position );
    }
  },
  {
    left: 0.5,
    top: 0,
    width: 0.5,
    height: 1,
    background: new THREE.Color( 0x000000 ),
    eye: [ 0, 0, 1000],
    up: [ 0, 0, 0 ],
    fov: 60,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.z -= mouseX * 0.005;
      camera.position.z = Math.max( Math.min( camera.position.z, far ), near );
      camera.lookAt( scene.position );
    }
  }
];

function updateView() {

  for ( var ii = 0; ii < 3; ++ii ) {
    var view = views[ii];
    var camera = view.camera;

    var left   = Math.floor( windowWidth  * view.left );
    var top    = Math.floor( windowHeight * view.top );
    var width  = Math.floor( windowWidth  * view.width  );
    var height = Math.floor( windowHeight * view.height );

    renderer.setViewport( left, top, width, height );
    renderer.setScissor( left, top, width, height );
    renderer.setScissorTest( true );
    renderer.setClearColor( view.background );

    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.render( scenes[ii], camera );

  }
}
