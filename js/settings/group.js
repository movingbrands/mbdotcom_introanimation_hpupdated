var GradientShader = function(){
    this.uniforms = THREE.UniformsUtils.merge([
		{
			"tMatCap"  : { type: "t", value: null },
			"resolution"  : { type: "v2", value: null },
			"time"  : { type: "f", value: null },
		}
    ]);
    this.vertexShader = [

      "varying vec2 vN;",

      "void main() {",

      "vec4 p = vec4( position, 1. );",
      "vec3 e = normalize( vec3( modelViewMatrix * p ) );",
      "vec3 n = normalize( normalMatrix * normal );",

      "vec3 r = reflect( e, n );",
      "float m = 2. * sqrt(pow( r.x, 2. ) + pow( r.y, 2. ) + pow( r.z + 1., 2. ));",
      "vN = r.xy / m + .5;",

      "gl_Position = projectionMatrix * modelViewMatrix * p;",

      "}"

    ].join("\n");

    this.fragmentShader = [

      "uniform sampler2D tMatCap;",
  		"varying vec2 vN;",

  		"void main() {",
    		"vec3 base = texture2D( tMatCap, vN ).rgb;",
    		"gl_FragColor = vec4( base, 1. );",
  		"}"

    ].join("\n");

    this.setUniforms = function(UNIFORMS) {
        for(u in UNIFORMS){;
            if (this.uniforms[u]) this.uniforms[u].value = UNIFORMS[u];
        }
    };
}

var uniforms = {
    "tMatCap": new THREE.ImageUtils.loadTexture( 'assets/h.jpg' ),
    "resolution": new THREE.Vector2(),
    "time": 0.0,
}


function Group(array1, array2, array3){

  var gradientShader = new GradientShader();
  gradientShader.setUniforms(uniforms);
  var matL = new THREE.ShaderMaterial({
    uniforms: gradientShader.uniforms,
        vertexShader: gradientShader.vertexShader,
        fragmentShader: gradientShader.fragmentShader,
        transparent: true,
  });


  var stopUp = false;
  var stopUp1 = false;

  var position = new THREE.Vector3( 1000,  -1050,  -200 );

  var msh = new THREE.Mesh();
  var loader =  new THREE.BufferGeometryLoader();
  loader.load( 'assets/untitled.json', function ( geo ) {

   msh.geometry = geo;
   msh.material = matL;
    msh.scale.set(2.4,2.4,2.4);
    msh.position.set(position.x,position.y,position.z);
    msh.material.needsUpdate = true;
    //msh = mesh.clone();
  });

  this.component = msh;

  this.update1 = function(){
    msh.rotation.y -= 0.01;
    msh.rotation.z += 0.001;

      if( msh.position.x > 1750){
        stopUp1 = true;
      }
      //
      if(stopUp1 == true){
        msh.position.x -= 3.1;
        msh.position.y += 1.9;
      }else{
        msh.position.x += 3.7;
      }



  }


  this.update2 = function(){

      msh.rotation.y += 0.01;
      msh.rotation.z += 0.001;
      if(msh.position.y > 450){
        stopUp = true;
      }

      if(stopUp == true){
        msh.position.x -= 3.1;
        msh.position.y -= 1.7;
      }else{
        msh.position.y += 3.6;
      }

  }

  this.update3 = function(){
      msh.rotation.y += 0.01;
      msh.rotation.x += 0.001;
      msh.position.x -= 3.1;
      msh.position.y += 1.9;
  }


}
