function preLoad(){

      var count = 4;
      for (var j = 0; j < 3; j++) {
        for (var i = 0; i < count; i++) {
           var panel = {
               texture: new THREE.TextureLoader().load("assets/textures/eir/" + i + ".png")
           }
           panel.texture.wrapS = THREE.RepeatWrapping;
           panel.texture.wrapT = THREE.RepeatWrapping;
           works1.push(panel);
        }
      }

      var count = 5;
      for (var j = 0; j < 2; j++) {
        for (var i = 0; i < count; i++) {
           var panel = {
               texture: new THREE.TextureLoader().load("assets/textures/housing/" + i + ".png")
           }
           panel.texture.wrapS = THREE.RepeatWrapping;
           panel.texture.wrapT = THREE.RepeatWrapping;
           works2.push(panel);
        }
      }

      var count = 6;
      for (var j = 0; j < 2; j++) {
        for (var i = 0; i < count; i++) {
           var panel = {
               texture: new THREE.TextureLoader().load("assets/textures/asana/" + i + ".png")
           }
           panel.texture.wrapS = THREE.RepeatWrapping;
           panel.texture.wrapT = THREE.RepeatWrapping;
           works3.push(panel);
        }
      }
    init();
}
