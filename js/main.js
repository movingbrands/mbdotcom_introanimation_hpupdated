			if(!Detector.webgl)
					Detector.addGetWebGLMessage();

			$( "#info" ).fadeIn( 1500, function() {
				setTimeout(function(){
				$( "#info" ).fadeOut( 1500 );
			  }, 5000);
			 });

			var container, camera, views, renderer;
			var windowWidth = window.innerWidth;
			var windowHeight = window.innerHeight;
			var mouseX = 0, mouseY = 0;

			var works1 = [], works2 = [], works3 = [];

			var elements = [];
			var scenes = [];

			preLoad();
			function init() {

				container = document.getElementById( 'container' );
				for (var ii =  0; ii < views.length; ++ii ) {
					var view = views[ii];
					camera = new THREE.PerspectiveCamera( view.fov, window.innerWidth / window.innerHeight, 1, 10000 );
					camera.position.fromArray( view.eye );
					camera.up.fromArray( view.up );
					view.camera = camera;
				}

				for(i = 0; i < 3; i++){
					var element = new Group( works1, works2, works3 );
					elements.push(element);
				}

				for(i = 0; i < 3; i++){
					var scene = new THREE.Scene();
					scene.add(elements[i].component);
					scenes.push(scene);
				}

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				container.appendChild( renderer.domElement );
				renderer.shadowMap.enabled = true;
				renderer.shadowMap.type = THREE.PCFShadowMap;

				document.addEventListener('mousedown', onMouseDown, false);
   			document.addEventListener('mouseup', onMouseUp, false);
				document.addEventListener('mousemove', onMouseMove, false );
				document.body.addEventListener( 'mousewheel', mousewheel, false );
				document.body.addEventListener( 'DOMMouseScroll', mousewheel, false ); // firefox
				window.addEventListener( 'resize', onWindowResize, false );

				animate();

			}

			function mousewheel( e ) {
			}

			function onMouseDown(event){

			}

			function onMouseUp(event){
			}


			function onMouseMove( event ) {
				mouseX = ( event.clientX - windowWidth / 2 );
				mouseY = ( event.clientY - windowHeight / 2 );

			}

			function onWindowResize() {
				windowHalfX = window.innerWidth / 2;
				windowHalfY = window.innerHeight / 2;
				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();
				renderer.setSize( window.innerWidth, window.innerHeight );
			}


			function animate() {
				render();
				requestAnimationFrame( animate );
			}

			function render() {

				updateView();
				elements[0].update1();
				elements[1].update2();
				elements[2].update3();

			}
